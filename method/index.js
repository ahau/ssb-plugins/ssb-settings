const CrutAuthors = require('ssb-crut-authors')
const Crut = require('ssb-crut')

const settingsSpec = require('../spec/settings')
const linkSpec = require('../spec/link')
const FindByFeedId = require('./find-by-feed-id')

module.exports = function Method (server) {
  const settingsCrut = new CrutAuthors(server, settingsSpec)
  const linkCrut = new Crut(server, linkSpec)
  const get = settingsCrut.read.bind(settingsCrut)

  return {
    create: settingsCrut.create.bind(settingsCrut),
    get,
    link: {
      create ({ settings: settingsId }, cb) {
        get(settingsId, (err, settings) => {
          if (err) return cb(err)

          const input = {
            parent: server.id,
            child: settingsId
          }

          // set the recps on the link to be the same as the recps on the settings
          if (settings.recps) input.recps = settings.recps

          linkCrut.create.bind(linkCrut)(input, cb)
        })
      }
    },
    findByFeedId: FindByFeedId(server, get),
    update: settingsCrut.update.bind(settingsCrut),
    tombstone: settingsCrut.tombstone.bind(settingsCrut)
  }
}

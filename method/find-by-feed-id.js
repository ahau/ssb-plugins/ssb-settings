const { isFeed } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')

module.exports = function FindByFeedId (ssb, getSettings) {
  const { where, and, type, author, toPullStream } = ssb.db.operators

  function getActiveSettings (settingsId, cb) {
    getSettings(settingsId, (err, settings) => {
      if (err) return cb(null, null)
      if (settings.tombstone) return cb(null, null)
      if (settings.states.length && settings.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" settings means no settings / tombstoned

      cb(null, settings)
    })
  }

  return function findByFeedId (feedId, opts = {}, cb) {
    if (typeof opts === 'function') return findByFeedId(feedId, {}, opts)
    if (!isFeed(feedId)) return cb(new Error('requires a valid feedId'))

    pull(
      ssb.db.query(
        where(
          and(
            author(feedId),
            type('link/feed-settings')
          )
        ),
        toPullStream()
      ),
      pull.filter(msg =>
        msg.value.content.parent === feedId && msg.value.content.tangles.link.root === null && msg.value.content.tangles.link.previous === null
      ),
      pull.map(link => link.value.content.child),
      paraMap(opts.getSettings || getActiveSettings, 4),
      pull.filter(Boolean),
      pull.collect((err, results) => {
        if (err) return cb(err)

        cb(null, results)
      })
    )
  }
}

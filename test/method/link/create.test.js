const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

test('settings.link.create and findByFeedId', { objectPrintDepth: 10 }, t => {
  const server = Server({ recpsGuard: true, debug: true })

  const settings = {
    authors: { add: [server.id] },
    keyBackedUp: false,
    recps: [server.id]
  }

  // Create settings
  server.settings.create(settings, (err, settingsId) => {
    t.error(err, 'create setting record')
    t.true(isMsgId(settingsId), 'settingId')

    server.settings.link.create({ settings: settingsId }, (err, linkId) => {
      t.error(err, 'create link')
      t.true(isMsgId(linkId), 'linkId')

      server.settings.findByFeedId(server.id, (err, allSettings) => {
        t.error(err, 'Find settings by feed id without error')

        const expectedSettings = [{
          key: settingsId,
          type: 'settings',
          originalAuthor: server.id,
          recps: [server.id],
          states: [],
          authors: {
            [server.id]: [{
              start: 0,
              end: null
            }]
          },
          conflictFields: [],
          keyBackedUp: false,
          tombstone: null
        }]

        t.deepEqual(expectedSettings, allSettings, 'Returned settings were correct')

        server.close()
        t.end()
      })
    })
  })
})

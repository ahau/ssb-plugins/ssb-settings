const test = require('tape')
const Server = require('../test-bot')
const { isMsgId } = require('ssb-ref')

test('settings.create (empty)', t => {
  t.plan(2)
  const server = Server()

  const details = {
    authors: {
      add: ['*']
    }
  }

  server.settings.create(details, (err, settingsId) => {
    t.error(err, 'Creates settings without error')

    t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')
    server.close()
    t.end()
  })
})

test('settings.create (key not backed up)', t => {
  t.plan(2)
  const server = Server()

  const details = {
    authors: {
      add: ['*']
    },
    keyBackedUp: false
  }

  server.settings.create(details, (err, settingsId) => {
    t.error(err, 'Creates settings without error')

    t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')
    server.close()
    t.end()
  })
})

test('settings.create (key backed up)', t => {
  t.plan(2)
  const server = Server()

  const details = {
    authors: {
      add: ['*']
    },
    keyBackedUp: true
  }

  server.settings.create(details, (err, settingsId) => {
    t.error(err, 'Creates settings without error')

    t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')
    server.close()
    t.end()
  })
})
